package movies.importer;
import java.io.IOException;

public class ProcessingTest {
	public static void main(String[] args) throws IOException{
		LowercaseProcessor lowercaseProc = new LowercaseProcessor("C:\\Users\\Luke\\Downloads\\Lab_5", "C:\\Users\\Luke\\Downloads\\Lab_5\\rmLowercase");
		lowercaseProc.execute();
		
		RemoveDuplicates rmDupes = new RemoveDuplicates("C:\\Users\\Luke\\Downloads\\Lab_5\\rmLowercase", "C:\\Users\\Luke\\Downloads\\Lab_5\\rmLowercase\\rmDupes");
		rmDupes.execute();
	}
}
