//Luke Weaver 1937361
package movies.importer;
import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String srcDir, String destDir){
		super(srcDir, destDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> arr){
		ArrayList<String> rmDupes = new ArrayList<String>();
		
		for(int i = 0; i < arr.size(); i++){
			if(!rmDupes.contains(arr.get(i))){
				rmDupes.add(arr.get(i));
			}
		}
		
		return rmDupes;
	}
}
