//Luke Weaver 1937361
package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	public LowercaseProcessor(String srcDir, String destDir){
		super(srcDir, destDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> arr){
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i = 0; i < arr.size(); i++){
			asLower.add(arr.get(i).toLowerCase());
		}
		
		return asLower;
	}
}
